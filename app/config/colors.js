export default {
    primary: "#5CE7AF",
    secondary: "#61B795",
    dark: "#2B3136",
    lightDark: "#3C424B",
    lightText: "#99A0A2",
    white: "#F7FFFC",
    lightGrey: "#444952",
    danger: "#E86046",
}