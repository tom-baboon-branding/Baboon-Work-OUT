import React from 'react'
import { TouchableHighlight } from 'react-native'
import { StyleSheet, Text, View } from 'react-native'
import Swipeable from 'react-native-gesture-handler/Swipeable'

import colors from '../config/colors'

export default function Excercise({title, onPress, renderRightActions}) {
    return (
        <Swipeable renderRightActions={renderRightActions}>
            <TouchableHighlight underlayColor={colors.lightGrey} onPress={onPress}>
                <View style={styles.container}>
                    <Text style={styles.title}>{title}</Text>
                </View>
            </TouchableHighlight>
        </Swipeable>
    )
}

const styles = StyleSheet.create({
    image: {
        width: 70,
        height: 70,
    },
    title: {
        paddingTop: 20,
        paddingBottom: 20,
        paddingLeft: 20,
        color: colors.white,
        borderBottomColor: colors.white,
        borderBottomColor: 'red',
        borderBottomWidth: 2,
        fontSize: 14
    }
})
