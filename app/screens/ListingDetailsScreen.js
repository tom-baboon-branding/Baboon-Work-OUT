import React, { useState } from 'react'
import { FlatList, StyleSheet, Text, View, SafeAreaView, StatusBar, Platform } from 'react-native'
import Excercise from '../components/Excercise'
import ExcerciseDeleteAction from '../components/ExcerciseDeleteAction'
import ListItemSeparator from '../components/ListItemSeparator'
import Screen from '../components/Screen'

import colors from '../config/colors'

const initialExercises = [
    {
        title: 'Bankdrukkenn',
        id: 1,
    },
    {
        title: 'Knallen',
        id: 2,
    },
    {
        title: 'Deadlift',
        id: 3,
    },
]

export default function ListingDetailsScreen({ title, subTitle, time }) {
    const [exercises, setExercises] = useState(initialExercises)
    const [refreshing, setRefreshing] = useState(false)

    const handleDelete = message => {
        // Delete the message
        setExercises(exercises.filter(m => m.id !== message.id));
    }
    return (
        <Screen>
            <View style={styles.wrapper}>
                <View style={styles.textWrapper}>
                    <Text style={styles.title}>{title}</Text>
                    <Text style={styles.time}>Last time: {time}</Text>
                    <Text style={styles.exercises}>Excercises:</Text>
                </View>

                <View>
                    <FlatList
                        data={exercises}
                        keyExtractor={exercise => exercise.id.toString()}
                        renderItem={({ item }) =>
                            <Excercise
                                title={item.title}
                                onPress={() => console.log('Tingelen Tatsen', item)}
                                renderRightActions={() =>
                                    <ExcerciseDeleteAction onPress={() => handleDelete(item)} />}
                            />
                        }
                        ItemSeparatorComponent={ListItemSeparator}
                        refreshing={refreshing}
                        onRefresh={() => {
                            console.log('Refresh')
                        }}
                    />
                </View>
            </View>
        </Screen>
    )
}

const styles = StyleSheet.create({
    col: {
        padding: 10,
        width: "50%",
        minHeight: 200,
    },
    wrapper: {
        backgroundColor: colors.lightDark,
        width: "100%",
        borderRadius: 12,
    },
    textWrapper: {
        padding: 20,
    },
    title: {
        fontSize: 24,
        color: colors.primary,
        fontWeight: "800",
        marginBottom: 10,
    },
    subTitle: {
        fontSize: 12,
        lineHeight: 18,
        color: colors.lightText,
        marginBottom: 10,
    },
    time: {
        fontSize: 10,
        color: colors.primary,
    },
    exercises: {
        fontSize: 18,
        marginTop: 30,
        marginBottom: 20,
        color: colors.white
    },
    container: {
        width: "100%",
    },
})
